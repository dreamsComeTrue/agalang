#ifndef _AGA_LEXER_H_
#define _AGA_LEXER_H_

#include <string>

namespace aga
{
	class agaLexer
	{
	public:
		agaLexer ();
		
		bool lex (std::string& source);
	};
}

#endif	//	_AGA_LEXER_H_