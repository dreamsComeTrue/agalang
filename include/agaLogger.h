#ifndef _AGA_LOGGER_H_
#define _AGA_LOGGER_H_

#include <string>

namespace aga
{
	class agaLogger
	{
	private:
		agaLogger () { }
		~agaLogger () { }
		
		agaLogger (agaLogger const&);       // Don't Implement
        void operator= (agaLogger const&); 	// Don't implement
		
	public:
		static agaLogger& getInstance ();
		
		static void log (const char* logInfo, bool endLine = true);
		static void log (std::string& logInfo, bool endLine = true);
		static void log (int logInfo, bool endLine = true);
	};
}

#endif	//	_AGA_LOGGER_H_