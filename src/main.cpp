#include <fstream>
#include <string>
#include <cctype>

#include "agaLexer.h"
#include "agaLogger.h"
#include "agaException.h"

/*
 * Program Entry point
 * 
 */
int main (int argc, char **argv) 
{	
	if (argc == 1)
	{
		aga::agaLogger::log ("Usage: agaLang source.aga");
		
		return - 1;
	}
	
	std::ifstream stream;

	stream.open (argv[1]);
	stream.unsetf (std::ios_base::skipws);

	if (!stream)
	{
		aga::agaLogger::log ("Unable to open the input file!"); 
		
		return -1;
	}

	std::string source = "";
	
	stream.seekg (0, std::ios::end);   
	source.reserve (stream.tellg ());
	stream.seekg (0, std::ios::beg);
	source.assign ((std::istreambuf_iterator<char> (stream)), std::istreambuf_iterator<char> ());
	stream.close ();
	
	aga::agaLexer lexer;
  
	lexer.lex (source);

	return 0;
}
