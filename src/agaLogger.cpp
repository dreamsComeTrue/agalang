#include <iostream>
#include <sstream>

#include "agaLogger.h"

namespace aga
{
	//--------------------------------------------------------------------------------
	
	agaLogger& agaLogger::getInstance ()
	{
		static agaLogger instance;
		
		return instance;
	}
	
	//--------------------------------------------------------------------------------
	
	void agaLogger::log (const char* logInfo, bool endLine)
	{
		std::cout << logInfo;
		
		if (endLine)
		{
			std::cout << std::endl;
		}
	}
	
	//--------------------------------------------------------------------------------
	
	void agaLogger::log (std::string& logInfo, bool endLine)
	{
		std::cout << logInfo;
		
		if (endLine)
		{
			std::cout << std::endl;
		}
	}	
	
	//--------------------------------------------------------------------------------
	
	void agaLogger::log (int logInfo, bool endLine)
	{
		std::stringstream ss;
		ss << logInfo;

		std::cout << ss.str();
		
		if (endLine)
		{
			std::cout << std::endl;
		}
	}		

	//--------------------------------------------------------------------------------
}
